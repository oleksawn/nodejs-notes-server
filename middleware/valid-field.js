const validUsername = (req, res, next) => {
  try {
    if (!req.body.username) {
      throw new Error('no username');
    } else if (!/^[a-z0-9\.\_.\-\!\#\^\~]{2,60}$/i.test(req.body.username)) {
      throw new Error(
          `username must consist of 2 up to 60, 
          and can contain latin letters, digits and characters . _ - ! # ^ ~ `,
      );
    }
    next();
  } catch (err) {
    res.status(400).json({
      message: err.message,
    });
  }
};

const validPassword = (req, res, next) => {
  try {
    password = req.body.password || req.body.newPassword;
    if (!password) {
      throw new Error('no password');
    } else if (
      !/^[a-z0-9\.\,\_\-\+\!\?\:\;\`\'\"\*\^\~\@\$\%\&\#\(\)]{2,60}$/i.test(
          password,
      )
    ) {
      throw new Error(
          `password must consist of 2 up to 60, 
        and can contain latin letters, digits 
        and characters .,_-+!?:;\`'"*^~@$%&#()`,
      );
    }
    /*
  else if (!/[a-z]{1,}/.test(req.body.password)) {
    throw new Error(
      'password must contain at least one lowercase letter'
    );
  } else if (!/[A-Z]{1,}/.test(req.body.password)) {
    throw new Error(
      'password must contain at least one uppercase letter'
    );
  } else if (!/[0-9]{1,}/.test(req.body.password)) {
    throw new Error('password must contain at least one digit');
  } else if (
    !/[\.\,\_\-\+\!\?\:\;\`\'\"\*\^\~\@\$\%\&\#\|\\\/\(\)\{\}\[\]\<\>]{1,}/
    .test(
      req.body.password
    )
  ) {
    throw new Error(
      'password must contain at least one character'
    );
  }
  */
    next();
  } catch (err) {
    res.status(400).json({
      message: err.message,
    });
  }
};

module.exports = {
  validUsername,
  validPassword,
};
