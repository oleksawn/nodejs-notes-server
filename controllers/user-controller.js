const User = require('../models/user-model');
const bcrypt = require('bcryptjs');

const getUser = async (req, res) => {
  console.log('user get');
  try {
    const {_id, username, createdDate} = await User.findById(req.body.id);
    res.status(200).json({user: {_id, username, createdDate}});
  } catch (err) {
    res.status(400).json({
      message: err.message,
    });
  }
};

const deleteUser = async (req, res) => {
  try {
    // delete all user's notes or ability to get back?
    await User.findByIdAndDelete(req.body.id);
    res.status(200).json({message: `User was deleted`});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
};

const changeUserPassword = async (req, res) => {
  try {
    const {id, oldPassword, newPassword} = req.body;

    if (oldPassword == newPassword) {
      throw new Error('you can not use old password');
    }

    const user = await User.findById(id);

    const isPasswordVerified = bcrypt.compareSync(oldPassword, user.password);
    if (!isPasswordVerified) {
      throw new Error('wrong password');
    }

    await User.findByIdAndUpdate(req.body.id, {
      password: bcrypt.hashSync(newPassword, 8),
    });

    res.status(200).json({message: `password changed`});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
};

module.exports = {
  getUser,
  deleteUser,
  changeUserPassword,
};
