const express = require('express');
const notesRouter = express.Router(); // eslint-disable-line new-cap
const {
  getAllNotes,
  createNote,
  getNote,
  changeNote,
  changeNoteDoneStatus,
  deleteNote,
} = require('../controllers/notes-controller');

notesRouter.get('/', getAllNotes);
notesRouter.post('/', createNote);
notesRouter.get('/:id', getNote);
notesRouter.put('/:id', changeNote);
notesRouter.patch('/:id', changeNoteDoneStatus);
notesRouter.delete('/:id', deleteNote);

module.exports = notesRouter;
