const express = require('express');
const userRouter = express.Router(); // eslint-disable-line new-cap
const {
  getUser,
  deleteUser,
  changeUserPassword,
} = require('../controllers/user-controller');
const {validPassword} = require('../middleware/valid-field');

userRouter.get('/me', getUser);
userRouter.delete('/me', deleteUser);
userRouter.patch('/me', validPassword, changeUserPassword);

module.exports = userRouter;
