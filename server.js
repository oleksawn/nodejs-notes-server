require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const {urlencoded} = require('express');
const cors = require('cors');
const morgan = require('morgan');

const userRouter = require('./routes/user-routes');
const notesRouter = require('./routes/notes-routes');
const authRouter = require('./routes/auth-routes');

const authMiddleware = require('./middleware/get-auth-token');
const {validUsername, validPassword} = require('./middleware/valid-field');

const PORT = process.env.PORT || 8080;
const app = express();

app.use(cors());
app.use(urlencoded({extended: false}));
app.use(express.json());
app.use(morgan('dev'));

app.use('/api/users', authMiddleware, userRouter);
app.use('/api/notes', authMiddleware, notesRouter);
app.use('/api/auth', validUsername, validPassword, authRouter);

mongoose
    .connect(
        process.env.MONGO_URL,
        {useNewUrlParser: true},
        {useUnifiedTopology: true},
    )
    .then(() => {
      console.log('mongo db connected');
      app.listen(PORT, () => {
        console.log(`server listen on port ${PORT}`);
      });
    })
    .catch((err) => {
      console.log('mongo db error', err);
    });
