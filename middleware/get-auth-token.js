const jwt = require('jsonwebtoken');
const User = require('../models/user-model');

module.exports = async (req, res, next) => {
  try {
    if (!req.headers.authorization) {
      throw new Error('User not authorised!');
    }
    const [, , authtoken] = req.headers.authorization
        .trim()
        .match(/^(JWT)[\s]{1,}([\S]{1,})/);

    const {id} = jwt.verify(authtoken, process.env.JWT_KEY);
    const user = await User.findById(id);
    if (!user) {
      throw new Error('no such user');
    }
    req.body.id = id;

    next();
  } catch (err) {
    res.status(400).json({
      message: err.message,
    });
  }
};
