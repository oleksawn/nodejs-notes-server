const Note = require('../models/note-model');

const getAllNotes = async (req, res) => {
  try {
    const count = await Note.find({userId: req.body.id}).count();
    const offset = Number(req.query.offset) || 0;
    const limit = Number(req.query.limit) || count;

    const notes = await Note.find({userId: req.body.id}, {__v: 0})
        .skip(offset)
        .limit(limit);

    res.status(200).json({offset, limit, count, notes});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
};

const createNote = async (req, res) => {
  try {
    if (!req.body.id || !req.body.text) {
      throw new Error('data required');
    }
    const note = new Note({
      userId: req.body.id,
      completed: false,
      text: req.body.text,
      createdDate: new Date(),
    });
    await note.save();
    res.status(200).json({message: 'Note was added'});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
};

const getNote = async (req, res) => {
  try {
    const {_id, userId, completed, text, createdDate} = await Note.findById(
        req.params.id,
    );
    if (!(userId == req.body.id)) {
      throw new Error('user do not have this note');
    }

    res
        .status(200)
        .json({note: {_id, userId, completed, text, createdDate}});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
};

const changeNote = async (req, res) => {
  try {
    const {userId} = await Note.findById(req.params.id);
    if (!(userId == req.body.id)) {
      throw new Error('user do not have this note');
    }

    if (!req.body.text) {
      throw new Error('data required');
    }

    await Note.findByIdAndUpdate(req.params.id, {text: req.body.text});
    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
};

const changeNoteDoneStatus = async (req, res) => {
  try {
    const {userId, completed} = await Note.findById(req.params.id);
    if (!(userId == req.body.id)) {
      throw new Error('user do not have this note');
    }

    if (completed) {
      await Note.findByIdAndUpdate(req.params.id, {completed: false});
      res.status(200).json({message: 'Success: note was unchecked'});
    } else {
      await Note.findByIdAndUpdate(req.params.id, {completed: true});
      res.status(200).json({message: 'Success: note was checked'});
    }
  } catch (err) {
    res.status(400).json({message: err.message});
  }
};

const deleteNote = async (req, res) => {
  try {
    const {userId} = await Note.findById(req.params.id);
    if (!(userId == req.body.id)) {
      throw new Error('user do not have this note');
    }

    await Note.findByIdAndDelete(req.params.id);
    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
};

module.exports = {
  getAllNotes,
  createNote,
  getNote,
  changeNote,
  changeNoteDoneStatus,
  deleteNote,
};
