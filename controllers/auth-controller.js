const bcrypt = require('bcryptjs');
const User = require('../models/user-model');
const jwt = require('jsonwebtoken');

const register = async (req, res) => {
  try {
    const {username, password} = req.body;

    const userFromDb = await User.findOne({username});
    if (userFromDb) {
      throw new Error(`${username} already exist`);
    }

    const user = new User({
      username,
      password: bcrypt.hashSync(password, 8),
      createdDate: new Date(),
    });
    await user.save();

    return res.status(200).json({message: 'Success'});
  } catch (err) {
    return res.status(400).json({message: err.message});
  }
};

const login = async (req, res) => {
  try {
    const {username, password} = req.body;

    const userFromDb = await User.findOne({username});
    if (!userFromDb) {
      throw new Error(`${username} does not exist`);
    }

    const isPasswordVerified = bcrypt.compareSync(
        password,
        userFromDb.password,
    );
    if (!isPasswordVerified) {
      throw new Error('wrong password');
    }

    const token = jwt.sign({id: userFromDb._id}, process.env.JWT_KEY, {
      expiresIn: '24h',
    });
    return res.status(200).json({message: 'success', jwt_token: token});
  } catch (err) {
    return res.status(400).json({message: err.message});
  }
};

module.exports = {
  register,
  login,
};
